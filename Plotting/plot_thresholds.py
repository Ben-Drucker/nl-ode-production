import matplotlib.pyplot as plt
from pandas import read_csv
from Utilities.color_utils import multi_dim_cmap, norm, decr_lum
import random
import numpy as np
import argparse
from os import chdir, name
from os.path import abspath

# Graphic parameters
plt.rcParams['font.family'] = "serif"
plt.rcParams['figure.figsize'] = [15, 5]

# OS check
if name == 'nt':
	print("This program is designed for UNIX-like systems (including mac). Windows is not supported.")

def get_contour_data():
	res = []
	chdir(abspath(__file__)[:-19]) # Change into the lower-level directory for consistency
	data = read_csv(open("../Data/threshold_values.csv", "r"))
	for i in range(len(data)):
		row = data.iloc[i]
		point_dict = {
			'couple12': row['couple12'],
			'couple21': row['couple21'],
			'hdenom': row['hdenom'],
			'AC': row['AC'],
			'DC': row['DC'],
			'bisection_precision_range': row['bisection_precision_range']
		}
		res.append(point_dict)
	
	# mySQL equivalent: `order by hdenom asc, couple12 asc, couple21 asc, DC asc`

	res.sort(key = lambda x: (x['hdenom'], x['couple12'], x['couple21'], x['DC']))
	
	return res

def graph_contour(couple_list, hdenom_list):
	global hDenom_label
	if len(couple_list) > 1 and len(hdenom_list) > 1:
		raise AssertionError("When trying to graph the contour plot, you attempted to pass a couple configuration list and hdenom list of sizes both greater than 1. This behavior is not supported.")

	random.seed(421)
	w, h = plt.figaspect(.75)
	fig = plt.figure(figsize=(w, h))

	data = get_contour_data()

	point = data[0]
	cur_conf = (point['couple12'], point['couple21'], float(point['hdenom']))
	
	while len(data) > 0:
		if (point['hdenom'] not in hdenom_list) or ((np.round(point['couple12'], 2), np.round(point['couple21'], 2)) not in couple_list):
			del data[0]
			if len(data) != 0:
				point = data[0]
			continue
		cur_conf = (point['couple12'], point['couple21'], point['hdenom'])
		point = data[0]
		x_data = []
		y_data = []
		y_err_data = []

		while cur_conf == (point['couple12'], point['couple21'], point['hdenom']):
			cur_conf = (point['couple12'], point['couple21'], point['hdenom'])
			if point['DC'] <= 30:
				x_data.append(point['DC'])
				y_data.append(point['AC'])
				y_err_data.append(2*point['bisection_precision_range'])
			del data[0]
			if len(data) != 0:
				point = data[0]
			else:
				break

		if len(couple_list) > 1:
			color_ = multi_dim_cmap((norm(cur_conf[1], 0.3, 0.9), norm(cur_conf[0], 0.3, 0.9)), decr_lum)
			label_ = r"$\kappa_{1\to2}=~$" + str((cur_conf[1], cur_conf[0]))

		else:
			color_ = [(cur_conf[2] - 3)/8 for x in range(3)]
			label_ = hDenom_label + str(cur_conf[2])
		
		plt.plot(x_data, y_data,marker = "o", markersize = 3.4, color = color_, linewidth=1.5, label = label_)
		cur_conf = (point['couple12'], point['couple21'], point['hdenom'])
		
	print("\033[93mPlotting legend. Comment out line below to remove.\033[0m")
	plt.legend()
	plt.title(r"\textbf{A}", loc = "left")
	plt.ylabel(r"$g_{\text{amp}}$ (nS)")
	plt.xlabel(r"$g_{\text{mean}}$ (nS)")
	plt.xlim(-1, 31)
	plt.ylim(0, 90)
	plt.show()

def main(couple_list, hdenom_list):
	cmd_parser = argparse.ArgumentParser()
	cmd_parser.add_argument("-L", action='store_true', required=False)
	args = vars(cmd_parser.parse_args())
	if args['L']:
		# Only use Latex if user explicitly asked for it
		plt.rcParams['text.usetex'] = True
		plt.rc('text.latex', preamble=r'\usepackage{amsmath}')
	else:
		cmd_parser.error("Must use -L LaTeX option in current version of program.\n"
						 "If LaTeX is not installed, see this webpage for instructions:\n"
						 "https://www.latex-project.org/get/\n")
	
	graph_contour(couple_list, hdenom_list)


if __name__ == "__main__":

	# Set the hDenom label:
	hDenom_label = r"$h_{\infty}$ slope = " # Only works with LaTeX

	# Uncomment to plot different coupling configurations at one hDenom
	couple_list=[(0.9, 0.5), (0.9, 0.9), (0.3, 0.3)]
	hdenom_list= [7.7]

	# Uncomment to plot hDenom configurations at one coupling configuration
	# couple_list=[(0.9, 0.5)]
	# hdenom_list= [3, 5, 7.7, 9]
	
	main(couple_list, hdenom_list)