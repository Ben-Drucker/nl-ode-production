import argparse
import matplotlib.pyplot as plt
import numpy as np
from Utilities.color_utils import multi_dim_cmap, norm, decr_lum
from math import sqrt
from pandas import read_csv
from os import chdir, name
from os.path import abspath
from colored_traceback import add_hook

add_hook()

# Graphic parameters
plt.rcParams['font.family'] = "serif"

# OS check
if name == 'nt':
	print("This program is designed for UNIX-like systems (including mac). Windows is not supported.")

global data
chdir(abspath(__file__)[:-11]) # Change into the lower-level directory for consistency
data = read_csv("../Data/IPD_values.csv")


def get_data(c12, c21, hDenom):
	global data

	df_subset = data[(data['couple12'] == c12) & (data["couple21"] == c21) & (data['hdenom'] == hDenom)]

	return [(row['IPD'], row['spk_rt'], row['spk_rt_SD'], row['num_aggregates']) for _, row in df_subset.iterrows()]

def main(c12_list, c21_list, hDenom_list):

	plot_title = r"\textbf{B}"
	hDenom_label = r"$h_{\infty}$ slope = " # Only works with LaTeX

	cmd_parser = argparse.ArgumentParser()
	cmd_parser.add_argument("-L", action='store_true')
	args = vars(cmd_parser.parse_args())
	if args['L']:
		# Only use Latex if user explicitly asked for it
		plt.rcParams['text.usetex'] = True
		plt.rc('text.latex', preamble=r'\usepackage{amsmath}')
	else:
		cmd_parser.error("Must use -L LaTeX option in current version of program.\n"
						 "If LaTeX is not installed, see this webpage for instructions:\n"
						 "https://www.latex-project.org/get/\n")

	_, main_graph_subplot = plt.subplots()
	main_graph_axis = main_graph_subplot
	main_graph_axis.set_xlabel("IPD (degrees)")
	main_graph_axis.set_ylabel("Spike Rate (spikes/sec)")
	main_graph_axis.set_xlim([-200, 200])
	main_graph_axis.set_ylim([0, 600])
	main_graph_axis.set_xticks(range(-180, 181, 90))
	main_graph_axis.set_title(plot_title, loc = "left")

	legend_square = np.zeros((len(c12_list), len(c21_list), 4))

	for i in range(len(c12_list)):
		for j in range(len(c21_list)):
			legend_square[i, j] = multi_dim_cmap((norm(c12_list[i], 0.3, 0.9), norm(c21_list[j], 0.3, 0.9)), decr_lum)

	legend_square_displ = np.zeros((7, 7, 4))
	for i in range(legend_square_displ.shape[0]):
		for j in range(legend_square_displ.shape[1]):
			legend_square_displ[i, j] = multi_dim_cmap((norm(0.3+i/10, 0.3, 0.9), norm(0.3+j/10, 0.3, 0.9)), decr_lum)

	for c12 in c12_list:
		for c21 in c21_list:
			for hDenom in hDenom_list:
				if c12 < c21: 
					continue
				res = get_data(c12, c21, hDenom)
				if len(res) == 0:
					continue
				res.sort()

				z_star = 1
				base_x = []
				base_y = []
				base_sd = []
				for x in res:
					base_x.append(360 * x[0])
					base_y.append(x[1])
					base_sd.append(z_star*x[2]/sqrt(x[3]))

				# Hemispheres
				III_x = [x - 180 for x in base_x]
				I_x = base_x
				III_y = [y for y in reversed(base_y)]
				I_y = base_y
				I_sd = base_sd

				if len(c12_list) > 1 or len(c21_list) > 1:
					color_ = multi_dim_cmap((norm(c21, 0.3, 0.9), norm(c12, 0.3, 0.9)), decr_lum)
					label_ = r"$\kappa_{1\to2}=~$" + str((c12, c21))
				else:
					color_ = [(hDenom - 3)/8 for x in range(3)]
					label_ = hDenom_label + str(hDenom)

				main_graph_axis.errorbar(III_x, III_y, color=color_, linestyle="dashed",
						marker="o", markersize=0, fillstyle="none",  capsize=2, capthick = .5, linewidth = 1.5)
				main_graph_axis.errorbar(I_x, I_y, yerr = I_sd, color=color_, linestyle="solid",
						marker="o", markersize=0, fillstyle="none", capsize=3.1, capthick = 1.5, linewidth = 1.5, label = label_)
	print("\033[93mPlotting legend. Comment out line below to remove.\033[0m")
	main_graph_axis.legend()
	plt.show()

if __name__ == "__main__":
	# Uncomment to plot multiple coupling configurations at one hDenom configuration
	# c12_list = [0.9, 0.5, 0.3] # Will try to find data for all combinations of c12 >= c21
	# c21_list = [0.9, 0.5, 0.3]
	# hDenom_list = [7.7]

	# # Uncomment to plot multiple hDenom configurations at one coupling configuration
	c12_list = [0.9] # Will try to find data for all combinations of c12 >= c21
	c21_list = [0.5]
	hDenom_list = [3, 5, 7.7, 9]
	

	main(c12_list, c21_list, hDenom_list)
