import matplotlib, numpy

def multi_dim_cmap(val, mix_func):

	colors_r_b = [(0, (0.2, 0.2, 1, 1)), (0.5, (0.75, 0.2, 0.75)), (1, (1, 0.2, 0.2, 1))]
	colors_blk_easy = [(0, (0.0, 0, 0.0, 0)), (0.5, (0.45, 0, 0.45)), (1, (0.9, 0, 0.9, 0))]

	axis_r_b = matplotlib.colors.LinearSegmentedColormap.from_list('colors_rb', colors_r_b, N = 1024)
	axis_darken = matplotlib.colors.LinearSegmentedColormap.from_list('axis_darken', colors_blk_easy, N = 1024)
	
	res = mix_func(numpy.asarray(axis_r_b(val[1])), numpy.asarray(axis_darken(val[0])))
	return res

def norm(X, mi = None, ma = None):
	if mi is None and ma is None:
		_min = X.min()
		_max = X.max()
	else:
		_max = ma
		_min = mi
		
	_range = _max - _min
	
	if mi is not None and ma is not None:
		try:
			for elt in numpy.nditer(X, op_flags=['readwrite']):
				elt[...] = (elt - _min)/_range
		except Exception:
			return (X - _min)/_range
		
	for elt in numpy.nditer(X, op_flags=['readwrite']):
		elt[...] = (elt - _min)/_range
	
	return X
 
def decr_lum(color_a, color_b): 
	color_a[0] = max(0, color_a[0]*(1-color_b[0]))
	color_a[2] = max(0, color_a[2]*(1-color_b[2]))
	return color_a
