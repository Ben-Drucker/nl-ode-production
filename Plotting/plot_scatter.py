from matplotlib import pyplot as plt
from matplotlib.colors import hex2color
import numpy as np
from Utilities.graphing_utils import txt_to_list
from scipy import linalg
from os import name

plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = 'serif'
plt.rc('text.latex', preamble=r'\usepackage{amsmath}')

# OS check
if name == 'nt':
	print("This program is designed for UNIX-like systems (including mac). Windows is not supported.")

dt = 1e-4

#print(nm[:100])

def scatter_plot(nm, label):
    t = [x/10000 for x in range(100000, 250000)]

    #print(t[:100])

    lcyc = .25/dt
    ncyc = int(len(t)/lcyc);

    xdc = []; xac = []
    for i in range(int(ncyc) - 1):
        nmcyc = nm[int(i*(lcyc)): int((i+1)*lcyc) + 1]
        xdc.append(np.mean(nmcyc))
        xac.append((max(nmcyc) - min(nmcyc))/2)

    plt.plot(xdc, xac, linewidth = 0, marker = 'o', markersize = 4, label = label)
    return xdc, xac

    
xdc_in, xac_in = scatter_plot(txt_to_list(0, 0, 1)[100000:], "In Phase")
xdc_out, xac_out = scatter_plot(txt_to_list(0, 0.5, 1)[100000:], "Out of Phase")


In = np.zeros(shape = (2, len(xdc_in)))
In[0] = (xdc_in)
In[1] = (xac_in)
In = In.T

Out = np.zeros(shape = (2, len(xdc_out)))
Out[0] = xdc_out
Out[1] = xac_out

Out = Out.T

In_mean = np.mean(In, axis = 0)
Out_mean = np.mean(Out, axis = 0)

Sin = np.matmul(In.T,In)
Sout = np.matmul(Out.T,Out)

S = Sin + Sout

#print(S)

#v = np.array([np.array(xdc_in).mean(), np.array(xac_in).mean()]) - np.array([np.array(xdc_out).mean(), np.array(xac_out).mean()])

#print(S.shape)

solution = np.linalg.solve(S, In_mean - Out_mean)
#print(solution)

orth = np.array([1, -solution[0]/solution[1]])

X = np.linspace(10, 30)

C = np.dot(solution,  0.5*(In_mean + Out_mean))
plt.ylim(0, 21)
plt.xlim(9, 32) 
plt.arrow(x=In_mean[0], y=1.5, dx=0, dy=-1, width=.08, color = "#ff7f0e")
plt.arrow(x=Out_mean[0], y=1.5, dx=0, dy=-1, width=.08, color = "#1f77b4")
plt.arrow(y=Out_mean[1], x=10.5, dx=-1, dy = 0, width=.08, color = "#ff7f0e")
plt.arrow(y=In_mean[1], x=10.5, dx=-1, dy = 0, width=.08, color = "#1f77b4")
plt.plot(X, orth[1]*X + C, color = "black", label = "FLD")
plt.title(r"\textbf{A}", loc = 'left')
plt.xlabel("Mean of "+r"$g_{\text{syn}}$")
plt.ylabel("Amplitude of "+r"$g_{\text{syn}}$")
plt.legend()

#print(linalg.solve(S, v))


plt.show()