from argparse import ArgumentParser
from os import chdir, name
from os.path import abspath
from matplotlib import colors, pyplot as plt
from numpy import zeros, round
from numpy.ma import masked_where
from pandas import read_csv
from pprint import pprint

plt.rcParams['font.family'] = "serif"

# OS check
if name == 'nt':
	print("This program is designed for UNIX-like systems (including mac). Windows is not supported.")

def main(hDenom):
	cmd_parser = ArgumentParser()
	cmd_parser.add_argument("-L", action='store_true', required=False)
	args = vars(cmd_parser.parse_args())
	if args['L']:
		# Only use Latex if user explicitly asked for it
		plt.rcParams['text.usetex'] = True
		plt.rc('text.latex', preamble=r'\usepackage{amsmath}')
	else:
		cmd_parser.error("Must use -L LaTeX option in current version of program.\n"
						 "If LaTeX is not installed, see this webpage for instructions:\n"
						 "https://www.latex-project.org/get/\n")


	global data
	X = [x/10 for x in range(1, 10)]
	X.sort()

	Y = [x/10 for x in range(1, 10)]
	Y.sort()

	matrix = zeros((9, 9))
	matrix = masked_where(matrix == 0, matrix)


	
	relevant_data = data[(data['hdenom'] == hDenom)]
	relevant_data.sort_values(by = ['couple12', 'couple21'])


	for _, row in relevant_data.iterrows():
		row = dict(row)
		matrix[int(row['couple21']*10) - 1, int(row['couple12']*10) - 1] = row['spk_delta']


	#matrix = masked_where(matrix is None, matrix)


	fig, heatmap_subplot = plt.subplots()
	heatmap_subplot.clear()

	spectrum = colors.LinearSegmentedColormap.from_list('custom_gs', [(0, (.9, .9, .9)), (1, (0.1, 0.1, 0.1))], N = 1024)

	heatmap_subplot.pcolormesh([x/10 for x in range(1, 10)], [x/10 for x in range(1, 10)], matrix, shading='nearest', cmap=spectrum)

	heatmap_subplot.spines['top'].set_visible(False)
	heatmap_subplot.spines['left'].set_visible(False)
	heatmap_subplot.spines['right'].set_visible(False)
	heatmap_subplot.spines['bottom'].set_visible(False)
	mapplable_custom = heatmap_subplot.collections[0]
	mapplable_custom.set_norm(colors.Normalize(vmin = 0, vmax = 500))
	c = plt.colorbar(mappable = heatmap_subplot.collections[0], ax = heatmap_subplot)
	c.set_label(r"$\Delta R$ (spikes/sec)", labelpad=10)

	heatmap_subplot.set_xlabel("Forward Coupling (" + r'$\kappa_{1\rightarrow 2}$' + ")")
	heatmap_subplot.set_ylabel("Backward Coupling (" + r'$\kappa_{2\rightarrow 1}$' + ")")
	heatmap_subplot.set_xticks([.1, .3, .5, .7, .9])
	heatmap_subplot.set_yticks([.1, .3, .5, .7, .9])
	heatmap_subplot.set_title(r"\textbf{D}", loc = "left")
	plt.show()


if __name__ == "__main__":
	chdir(abspath(__file__)[:-len("plot_heatmap.py")]) # Change into the lower-level directory for consistency
	global data
	data = read_csv("../Data/delta_values.csv")
	hDenom = 7.7
	main(hDenom)