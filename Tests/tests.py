from os import chdir, system as runcmdline
from os.path import abspath, join
import sys
sys.path.insert(1, '../')
sys.path.insert(1, '../Plotting')
from Plotting import plot_traces

def run_cmd(cmd):
    print("--->", cmd)
    if "python3" not in cmd:
        exec(cmd)
    else:    
        runcmdline(cmd)

def tests():
    
    chdir(join(abspath(__file__)[:-14], 'Plotting'))
    test_1 = "python3 plot_traces.py -L --mode syn"
    test_2 = "python3 plot_traces.py -L --mode sin --init-values run-before"
    test_3 = "python3 plot_traces.py -L --mode sin --init-values no-input"
    test_4 = "python3 plot_scatter.py -L"
    test_5 = "python3 plot_thresholds.py -L"
    test_6 = "python3 plot_IPD.py -L"
    test_7 = "python3 plot_heatmap.py -L"
    test_8 = "print(plot_traces.get_gNa_value(0.9, 0.5, 7.7))"
    test_9 = "print(plot_traces.get_gNa_value(0.9, 0.91, 7.7))"
    
    
    
    tests = [test_1, test_2, test_3, test_4, test_5, test_6, test_7, test_8, test_9]

    for test in tests:
        run_cmd(test)


if __name__ == "__main__":
    tests()