# Git actions to use:

- `git clone https://gitlab.com/Ben-Drucker/nl-ode-production`: copy the current state of the repo to your computer.
- `git pull`: update the repository
- `git branch`: view git branches
- `git checkout <branchname>`: move to branch `<branchname>`

# Dependency installation

From the root-level directory, run 

`pip3 install -r requirements.txt`

# Directory Description
#### [Directories listed in boldface]
- **NL-ODE-PRODUCTION**
  - **Plotting** - Contains all plotting programs
    - **Utilities** - Contains some necessary utilities for making visualizations. Not necessary to run directly.
    - `plot_heatmap.py` - Plot IPD spike rate difference heat map
    - `plot_threshold.py` - Plot repetitive firing threshold curves
    - `plot_IPD.py` - Plot IPD tuning curves
    - `plot_traces.py` - Plot voltage traces based on parameters (edited in file). Previously called simplifiedODE.py.
    - `plot_scatter.py` - Plot scatterplot with in-phase and out-of-phase datapoints and FLD line
  - **Data** - Contains all data. File names are self-explanatory.
  - **Figures** - Contains figure outputs (currently only from `plot_traces.py`)
  - **C** - Contains all C-related files, binaries, and data
  - **Jupyter Scripts** - Contains data preprocessing scrips. (Not necessary to run as user)
  - **Tests** - Contains tests. Must be in Tests directory to run these.

# Instructions for Running

## `plot_traces.py`:

One can edit desired AC and DC values, coupling configurations, and hDenom values by directly editing the 

    if __name__ == "__main__"

section.

### Formally

`python3 plot_traces --mode <plotting mode> --init-values <initial value mode> [-L]`

  - ... where `<plotting mode>` is either `sin` for sinusoidal inputs or `syn` for synaptic inputs <br>
  - ... where (for the `sin` mode only), `<initial value mode>` is either `no-input` to generate plots with initial values obtained from a no-input run OR `run-before` to generate plots with initial values obtained by running with high `AC` value first.<br>
  - ... where `-L` is either present or absent. If this flag is present, the program will use $\LaTeX$ to render graph text. Currently, this flag must be used for proper behavior.

### Examples (assuming in the root-level directory)

- To plot using synaptic input, run 

    `python3 Plotting/plot_traces.py --mode syn -L`

    Note that we use `Plotting/` since we assume we're in the root-level directory. If we `cd` into Plotting, we don't need this.

- To plot using sinusoidal input using no pre-run initial values, run

    `python3 Plotting/plot_traces.py --mode sin --init-values no-input -L`

- To plot using sinusoidal input using initial values obtained by an initial run, run

    `python3 Plotting/plot_traces.py --mode sin --init-values run-before -L`

### Functions in plot_traces (optional reading)

- `main_sin(param_dict)`: Plot sinusoidal input voltage trace with parameters in `param_dict`
- `main_syn(iteration, param_dict`): Plot synaptic input voltage trace with parameters in `param_dict` and with `NM_lib` index `iteration`
- `get_gNa_value(c12, c21, hDenom)`: Retrieve gNa value with desired coupling and hDenom values
- `twoCptODE(t,x,couple12,couple21,DC,AC,f, gna, hDenom_)`: Get points on the estimated two compartment ODE solution function, with desired parameters
- `txt_to_list(iteration, IPD, lin_interp_fact)`: Get a list representing the synaptic input value over time based on desired `NM_lib` index `iteration`, IPD value, and linear interpolation factor
- `make_plot(t, v1, v2, couple12, couple21, hDenom, AC = None, DC = None, Iteration = None)`: Save a plot originating from input parameters

## `plot_heatmap.py`, `plot_IPD.py`, `plot_scatter.py`:

In all of these programs, one may adjust which data they want to plot by editing the file's

    if __name__ == "__main__"

section.
### Formally

`python3 <script_name.py> [-L]`

  - ... where `<script_name.py>` is one of {`plot_heatmap.py`, `plot_IPD.py`, `plot_scatter.py` } <br>
  - ... where `-L` is either present or absent. If this flag is present, the program will use $\LaTeX$ to render graph text. Currently, this flag must be used for proper behavior.

## `plot_scatter.py`

This file has no options. One can run it simply with `python3 plot_scatter.py -L`.
